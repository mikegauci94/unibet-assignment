(function ($) {
  if (jQuery().slick) {
    $(".unibet-simple-slider-section").each(function () {
      $(this).slick({
        autoplay: true,
        autoplaySpeed: 3000,
        speed: 500,
        dots: true,
      });
    });
  }
})(jQuery);
