
<?php 
$slide = (int) $slide;
$unibet_slides_content = get_post_meta($slide, 'unibet_slides_content', true);
if ( !empty($unibet_slides_content) && is_array($unibet_slides_content) ) { 
    ?>

<div class="unibet-simple-slider-section unibet-slides-alignfull">
<?php
    foreach ( $unibet_slides_content as $slide ) {
        $slide_link = $slide['slide_button_link'] ?? '';
        if ( !empty($slide_link) ) {
            $slide_link = esc_url($slide_link);
        } else {
            $slide_link = '#';
        }        
        $slide_image = '';
        $slide_image_id = (int) $slide['slide_image'];

        if ( !empty($slide_image_id) ) {
            $slide_image = wp_get_attachment_url( $slide_image_id );
        }
?>
    <div class="unibet-slides-img-wrapper">
        <div class="unibet-slider-overlay"></div>
        <img src="<?php echo $slide_image; ?>" alt="">
        <div class="unibet-slides-container">
            <div class="unibet-slides-content">
                <h1  class="unibet-slider-heading"><?php echo $slide['slide_title'] ?? ''; ?></h1>
                <p  class="unibet-slider-text"><?php echo $slide['slide_description'] ?? ''; ?></p>
                <button class="unibet-slider-button" tabindex="-1"><a href="<?php echo $slide_link; ?>" target="_blank" rel="nofollow" tabindex="-1"><?php echo $slide['slide_button'] ?? ''; ?></a></button>
            </div>
        </div>
    </div>
<?php } ?>
</div>
<?php
}
?>