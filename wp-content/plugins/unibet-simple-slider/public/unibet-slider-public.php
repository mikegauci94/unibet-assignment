<?php 

/**
 * Enqueue Admin Scripts
 *
 * @return void
 */
function unibet_enqueue_scripts() {
    global $post;
    if ( has_shortcode($post->post_content, 'mycarousel') ) {
        wp_enqueue_style('slick',  UNIBET_SLIDER_URL . 'public/assets/css/slick.min.css', [], '1.8.0');
        wp_enqueue_style( 'unibet-slider-style', UNIBET_SLIDER_URL . 'public/assets/css/unibet-simple-slider.css', ['slick'], '1.0');

        wp_enqueue_script('slick',  UNIBET_SLIDER_URL . 'public/assets/js/slick.min.js', ['jquery'], '1.8.0');
        wp_enqueue_script( 'unibet-slider-script', UNIBET_SLIDER_URL . 'public/assets/js/unibet-simple-slider.js', ['jquery', 'slick'], '1.0', true);
    }
}
add_action('wp_enqueue_scripts', 'unibet_enqueue_scripts');



/**
 * Include Template file
 *
 * @param string $template
 * @param array $template_params
 * 
 * @return void
 */
function unibet_public_template($template, $template_params = []) {
    $template_path = UNIBET_SLIDER_PATH . 'public/templates/' . $template . '.php';
    $template_path = str_replace(['/', '\/'], DIRECTORY_SEPARATOR, $template_path);
    extract($template_params);
    if ( file_exists($template_path) ) {
        include $template_path;
    }
}


/**
 * Register the slider shortcode
 * 
 * @param  string $atts   Shortcode Attributes
 * @return string   HTML template of slider
 */
function unibet_simple_sider_shortcode($atts) {

    if ( !isset($atts['slide']) || empty($atts['slide']) || get_post_type($atts['slide']) != 'unibet_slides' ) {
        return '';        
    }

    ob_start();
    unibet_public_template('template-slider', $atts);
    return ob_get_clean();
}


add_shortcode('mycarousel', 'unibet_simple_sider_shortcode');