(function ($) {
  $("#add-slide").on("click", function () {
    var row = $(".empty-row.screen-reader-text").clone(true);
    row.removeClass("empty-row screen-reader-text");
    row.appendTo($("#unibet-slider-content-wrap"));
    return false;
  });

  $(".remove-slide").on("click", function () {
    $(this).parents(".slider-repeater-field").remove();
    return false;
  });

  $(document).on("click", ".slide-select-button, .slide-image-preview img", function (e) {
    e.preventDefault();

    var $button = $(this);

    if ($(e.target).is("img")) {
      $button = $(this).parents(".slide-image-preview").siblings("button");
    }

    var custom_uploader = wp
      .media({
        title: "Select Slide Image",
        library: {
          // uploadedTo : wp.media.view.settings.post.id, // attach to the current post?
          type: "image",
        },
        button: {
          text: "Use this image", // button label text
        },
        multiple: false,
      })
      .on("select", function () {
        // it also has "open" and "close" events
        var attachment = custom_uploader.state().get("selection").first().toJSON();
        $button.parents(".slide_image_content").siblings('input[type="hidden"]').val(attachment.id);
        $button
          .siblings(".slide-image-preview")
          .html('<img src="' + attachment.url + '">')
          .next()
          .show()
          .next()
          .val(attachment.id);
      })
      .open();
  });

  $(".slide-shortcode").on("click", function (e) {
    e.target.select();
  });

  if (jQuery().sortable) {
    $("#unibet-slider-content-wrap").sortable();
  }
})(jQuery);
