<?php
wp_nonce_field( 'unibet_slides_meta_box_nonce', 'unibet_slides_meta_box_nonce' );
$unibet_slides_content = get_post_meta($post->ID, 'unibet_slides_content', true);
?>

<div id="unibet-slider-content-wrap">
    <?php
     if ( !empty($unibet_slides_content) ) :
      foreach ( $unibet_slides_content as $key => $field ) {
          
          $image_link = '';
          
          if ( isset($field['slide_image']) ) {
              $image_link = wp_get_attachment_url(  (int) $field['slide_image'] );
          }
    ?>
    <div class="slider-repeater-field">
        <div class='slide-field-item'>
            <input 
                type="hidden" 
                name="slide_images[]" 
                placeholder="<?php _e("Slide Image", 'unibet-simple-slider'); ?>"
                value="<?php echo $field['slide_image'] ?? ''; ?>">
            <div class="slide_image_content">
                <div class="slide-image-preview">
                    <?php if ( $image_link ) { ?>
                    <img src="<?php echo $image_link; ?>">
                    <?php } ?>
                </div>
                <button type="button" class="slide-select-button button button-secondary"> <?php _e("Slide Image", 'unibet-simple-slider'); ?> </button>
            </div>
        </div>

        <div class='slide-field-item'>
            <div  class="field-label-text">
                <?php _e("Slide Title", "unibet-simple-slider"); ?>
            </div>
            <input 
                type="text" 
                name="slide_titles[]" 
                placeholder="<?php _e("Slide Title", 'unibet-simple-slider'); ?>"
                value="<?php echo $field['slide_title'] ?? ''; ?>">
        </div>

        <div class='slide-field-item'>
            <div class="field-label-text">
                <?php _e("Slide Description", "unibet-simple-slider"); ?>
            </div>
            <textarea 
                name="slide_descriptions[]"
                placeholder="<?php _e("Slide Description", 'unibet-simple-slider'); ?>" 
                rows="4"><?php echo $field['slide_description'] ?? ''; ?></textarea>
        </div>

        <div class='slide-field-item'>
            <div class="field-label-text">
                <?php _e("Slide Button Text", "unibet-simple-slider"); ?>
            </div>
            <input 
                type="text" 
                name="slide_buttons[]" 
                placeholder="<?php _e("Slide Button Text", 'unibet-simple-slider'); ?>" 
                value="<?php echo $field['slide_button'] ?? ''; ?>"/>
        </div>
        
        <div class='slide-field-item'>
            <div class="field-label-text">
                <?php _e("Slide Button Link", "unibet-simple-slider"); ?>
            </div>
            <input
                type="text"
                name="slide_button_links[]"
                placeholder="<?php _e("Slide Button Link", 'unibet-simple-slider'); ?>"
                value="<?php echo $field['slide_button_link']; ?>" />
        </div>
        <button class="remove-slide button button-secondary"><?php _e("Remove Slide", 'unibet-simple-slider'); ?> </button>
    </div>
    <?php
    }
    else :
    ?>
    <div class="slider-repeater-field">
        <div class='slide-field-item'>
            <input 
                type="hidden" 
                name="slide_images[]" 
                placeholder="<?php _e("Slide Image", 'unibet-simple-slider'); ?>">
            <div class="slide_image_content">
                <div class="slide-image-preview"></div>
                <button type="button" class="slide-select-button button button-secondary"> <?php _e("Slide Image", 'unibet-simple-slider'); ?> </button>
            </div>
        </div>


        <div class='slide-field-item'>
            <div  class="field-label-text">
                <?php _e("Slide Title", "unibet-simple-slider"); ?>
            </div>
            <input type="text" name="slide_titles[]" placeholder="<?php _e("Slide Title", 'unibet-simple-slider'); ?>">
        </div>

        <div class='slide-field-item'>
            <div class="field-label-text">
                <?php _e("Slide Description", "unibet-simple-slider"); ?>
            </div>
            <textarea name="slide_descriptions[]" placeholder="<?php _e("Slide Description", 'unibet-simple-slider'); ?>" rows="4"></textarea>
        </div>
        <div class='slide-field-item'>
            <div class="field-label-text">
                <?php _e("Slide Button Text", "unibet-simple-slider"); ?>
            </div>
            <input type="text" name="slide_buttons[]" placeholder="<?php _e("Slide Button Text", 'unibet-simple-slider'); ?>" />
        </div>

        <div class='slide-field-item'>
            <div class="field-label-text">
                <?php _e("Slide Button Link", "unibet-simple-slider"); ?>
            </div>
            <input type="text" name="slide_button_links[]" placeholder="<?php _e("Slide Button Link", 'unibet-simple-slider'); ?>" />
        </div>
    </div>
    <?php endif; ?>

    <!-- empty hidden one for jQuery -->
    <div class="slider-repeater-field empty-row screen-reader-text">
        <div class='slide-field-item'>
            <input 
                type="hidden" 
                name="slide_images[]" 
                placeholder="<?php _e("Slide Image", 'unibet-simple-slider'); ?>">
            <div class="slide_image_content">
                <div class="slide-image-preview"></div>
                <button type="button" class="slide-select-button button button-secondary"> <?php _e("Slide Image", 'unibet-simple-slider'); ?> </button>
            </div>              
        </div>


        <div class='slide-field-item'>
            <div  class="field-label-text">
                <?php _e("Slide Title", "unibet-simple-slider"); ?>
            </div>
            <input type="text" name="slide_titles[]" placeholder="<?php _e("Slide Title", 'unibet-simple-slider'); ?>">
        </div>

        <div class='slide-field-item'>
            <div class="field-label-text">
                <?php _e("Slide Description", "unibet-simple-slider"); ?>
            </div>
            <textarea name="slide_descriptions[]" placeholder="<?php _e("Slide Description", 'unibet-simple-slider'); ?>" rows="4"></textarea>
        </div>
        <div class='slide-field-item'>
            <div class="field-label-text">
                <?php _e("Slide Button Text", "unibet-simple-slider"); ?>
            </div>
            <input type="text" name="slide_buttons[]" placeholder="<?php _e("Slide Button Text", 'unibet-simple-slider'); ?>" />
        </div>

        <div class='slide-field-item'>
            <div class="field-label-text">
                <?php _e("Slide Button Link", "unibet-simple-slider"); ?>
            </div>
            <input type="text" name="slide_button_links[]" placeholder="<?php _e("Slide Button Link", 'unibet-simple-slider'); ?>" />
        </div>
    </div>
</div>
<p>
    <a id="add-slide" class="button button-primary" href="#">Add Slide</a>
</p>