<?php 

/**
 * Enqueue Admin Scripts
 *
 * @return void
 */
function unibet_enqueue_admin_scripts() {
    if ( isset($_GET['post_type']) && $_GET['post_type'] == 'unibet_slides' || ( isset($_GET['post']) && get_post_type((int) $_GET['post']) == 'unibet_slides' ) ) {
        wp_enqueue_media();  
        wp_enqueue_script( 'unibet-slider-admin-script', UNIBET_SLIDER_URL . 'admin/assets/js/unibet-simple-slider-admin.js', ['jquery'], '1.0', true);
        wp_enqueue_style( 'unibet-slider-admin-style', UNIBET_SLIDER_URL . 'admin/assets/css/unibet-simple-slider-admin.css', [], '1.0');
    }
}
add_action('admin_enqueue_scripts', 'unibet_enqueue_admin_scripts');

/**
 * Include Template file
 *
 * @param string $template
 * @param array $template_params
 * 
 * @return void
 */
function unibet_admin_template($template, $template_params = []) {
    $template_path = UNIBET_SLIDER_PATH . 'admin/templates/' . $template . '.php';
    $template_path = str_replace(['/', '\/'], DIRECTORY_SEPARATOR, $template_path);
    extract($template_params);
    if ( file_exists($template_path) ) {
        include $template_path;
    }
}

/**
 * Register Custom post type for slides
 *
 * @return void
 */
function unibet_simple_sider_register_cpt() {

	/**
	 * Post Type: Sliders.
	 */

	$labels = [
		"name" => __( "Sliders", "unibet-simple-slider" ),
		"singular_name" => __( "Slider", "unibet-simple-slider" ),
        "add_new_item" => __("Add new Slider", 'unibet-simple-slider'),
        "add_new" => __("Add new Slider", 'unibet-simple-slider'),
        "view_item" => __("View Slider", 'unibet-simple-slider'),
        "view_items" => __("View Sliders", 'unibet-simple-slider'),
	];

	$args = [
		"label" => __( "Sliders", "unibet-simple-slider" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => false,
		"show_ui" => true,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"delete_with_user" => false,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => [ "slug" => "unibet_slides", "with_front" => true ],
		"query_var" => true,
		"menu_icon" => "dashicons-cover-image",
		"supports" => [ "title", 'custom-fields' ],
		"show_in_graphql" => false,
	];

	register_post_type( "unibet_slides", $args );
}

add_action( 'init', 'unibet_simple_sider_register_cpt' );


/**
 * Register admin columns
 *
 * @param array $columns
 * @return array
 */
function unibet_slides_register_admin_columns( $columns) {

  unset($columns['date']);

  $columns['shortcode'] = __("Shortcode", 'unibet-simple-slider');
  return $columns;
}
add_filter( 'manage_unibet_slides_posts_columns', 'unibet_slides_register_admin_columns' );


/**
 * Manage Admin columns content of slider
 *
 * @param string $column
 * @param int $post_id
 * 
 * @return void
 */
function unibet_slides_posts_column_content( $column, $post_id ) {

    // Shortcode column
    if ( 'shortcode' === $column && get_post_status($post_id) == 'publish' ) {
      $shortcode_text = '[mycarousel slide='. $post_id .']';
      echo  sprintf('<div><input type="text" class="slide-shortcode" value="%s" readonly/></div>', $shortcode_text);
    }
}
add_action( 'manage_unibet_slides_posts_custom_column', 'unibet_slides_posts_column_content', 10, 2);


/**
 * Register admin metaboxes under slides post type
 *
 * @return void
 */
function unibet_slider_register_metaboxes() {
    add_meta_box(
        'slider-content', 
        __("Slides Content", 'unibet-simple-slider'), 
        'unibet_slider_content_metabox_html', 
        ['unibet_slides'], 'normal', 
        'default'
    );
}
add_action('admin_init', 'unibet_slider_register_metaboxes', 2);


/**
 * Content of slides-content metabox
 *
 * @return void
 */
function unibet_slider_content_metabox_html() {
    global $post;
    unibet_admin_template('metaboxes/slides-content-metabox', compact('post'));
}

add_action('save_post', 'unibet_save_sliders_content');


/**
 * Save the Slides Content on Post Save
 *
 * @param int $post_id
 * @return void
 */
function unibet_save_sliders_content($post_id) {
   
    if ( ! isset( $_POST['unibet_slides_meta_box_nonce'] ) ||
    ! wp_verify_nonce( $_POST['unibet_slides_meta_box_nonce'], 'unibet_slides_meta_box_nonce' ) ) {
        return;
    }

    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
        return;
    }

    if (!current_user_can('edit_post', $post_id)) {
        return;
    }

    $old_slides = get_post_meta($post_id, 'unibet_slides_content', true);

    $new_slides = array();

    $slide_titles = $_POST['slide_titles'] ?? array();
    $slide_descriptions = $_POST['slide_descriptions'] ?? array();
    $slide_images = $_POST['slide_images'] ?? array(); 
    $slide_buttons = $_POST['slide_buttons'] ?? array();
    $slide_button_links = $_POST['slide_button_links'] ?? array();

    $count = count( $slide_titles );
    
     for ( $i = 0; $i < $count; $i++ ) {


        if ( $slide_titles[$i] != '' ) :
           
            $new_slides[$i]['slide_title'] = sanitize_text_field ( $slide_titles[$i] );
            $new_slides[$i]['slide_image'] = intval ( $slide_images[$i] );
            $new_slides[$i]['slide_description'] = wp_kses_post( $slide_descriptions[$i] ); // and however you want to sanitize
            $new_slides[$i]['slide_button'] = wp_kses_post( $slide_buttons[$i] ); // and however you want to sanitize
            $new_slides[$i]['slide_button_link'] = wp_kses_post( $slide_button_links[$i] ); // and however you want to sanitize

        endif;
    }


    if ( !empty( $new_slides ) && $new_slides != $old_slides ) {
        update_post_meta( $post_id, 'unibet_slides_content', $new_slides );
    } elseif ( empty($new_slides) && $old_slides ) {
        delete_post_meta( $post_id, 'unibet_slides_content', $old_slides );
    }
}