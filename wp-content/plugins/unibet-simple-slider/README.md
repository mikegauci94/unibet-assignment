=== Plugin Name ===
Contributors: (this should be a list of wordpress.org userid's)
Donate link: mikegauci.com
Tags: comments, spam
Requires at least: 3.0.1
Tested up to: 3.4
Stable tag: 4.3
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

The Unibet Simple Slider plugin enables the editor hero sliders on any page or posts by inserting a unique generated shortcode.

== Description ==

The plugin foundations was built on [WORDPRESS PLUGIN BOILERPLATE GENERATOR](https://wppb.me/). The resourses on how the plugin was built are as follows:

* [WordPress Plugin Handbook](https://developer.wordpress.org/plugins/)
* [How to build a Slideshow Plugin](https://code.tutsplus.com/tutorials/build-a-slideshow-plugin-for-wordpress--wp-25789)
* [Custom fields and how to use get_post_meta](https://nicolamustone.blog/2017/03/01/post-meta-get-value/)
* [Adding Custom Meta Boxes to WordPress Admin Interface](https://www.sitepoint.com/adding-custom-meta-boxes-to-wordpress/)
* [How to to Add a Custom Media Uploader Button in WordPress Admin](https://rudrastyh.com/wordpress/customizable-media-uploader.html)
* [9 Tips for WordPress Plugin Development](https://www.webfx.com/blog/web-design/wordpress-plugin-development-tips/)
* [How to Create Custom Post Types in WordPress](https://www.wpbeginner.com/wp-tutorials/how-to-create-custom-post-types-in-wordpress/)
* [How to Create a Custom WordPress Plugin From Scratch](https://webdesign.tutsplus.com/tutorials/create-a-custom-wordpress-plugin-from-scratch--net-2668)
* [WordPress Shortcodes](https://kinsta.com/blog/wordpress-shortcodes/)
* [WordPress Hooks and Filters](https://codex.wordpress.org/Plugin_API#Hooks.2C_Actions_and_Filters)
* [Create a WordPress plugin from Scratch](https://www.youtube.com/watch?v=34HJlgkxieg&list=RDCMUCbmBY_XYZqCa2G0XmFA7ZWg&start_radio=1&rv=34HJlgkxieg&t=877&ab_channel=AlessandroCastellani)
* [jQuery sortable](https://jqueryui.com/sortable/)

== Installation ==

This section describes how to install the plugin and get it working.

e.g.

1. Upload `unibet-simple-slider.zip` zip file to the `/wp-content/plugins/` directory
1. Activate the plugin through the 'Plugins' menu in WordPress
1. Start populating the slider content buy uploading an image, input text and button links
1. Copy the generated shortcode
1. Paste it in any page or post.
1. Works with both Classic and Gutenberg editors

== Changelog ==

= 1.0 =
* Plugin launch initialize