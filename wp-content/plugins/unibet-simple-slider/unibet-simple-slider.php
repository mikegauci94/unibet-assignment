<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              mikegauci.com
 * @since             1.0.0
 * @package           Unibet_Simple_Slider
 *
 * @wordpress-plugin
 * Plugin Name:       Unibet Simple Slider
 * Plugin URI:        unibet.com
 * Description:       A simple slider mainly used for hero sections.
 * Version:           1.0.0
 * Author:            Michael Gauci
 * Author URI:        https://mikegauci.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       unibet-simple-slider
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

 define (  'UNIBET_SLIDER_PATH',  plugin_dir_path(__FILE__) );
 define (  'UNIBET_SLIDER_URL',  plugin_dir_url(__FILE__) );


 require UNIBET_SLIDER_PATH . 'admin'. DIRECTORY_SEPARATOR . 'unibet-slider-admin.php';
 require UNIBET_SLIDER_PATH . 'public'. DIRECTORY_SEPARATOR . 'unibet-slider-public.php';