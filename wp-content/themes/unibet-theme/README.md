## UniBet Theme | Assignment by Michael Gauci | Kindred Group PLC

This is a WordPress Theme built with Starter Bootstrap by them.es. [https://them.es/starter-bootstrap](https://them.es/starter-bootstrap)

## What's included in the finished theme?
* WordPress Theme
* Bootstrap Framework
* Sass Source files
* gulp + webpack configuration
* NPM configuration
* 5 Menus (4 Header, 1 Footer)
* Advanced Custom Fields Pro plugin (ACF)
* Slick Slider [https://kenwheeler.github.io/slick/](https://kenwheeler.github.io/slick)
* Hero Slider built from ACF

## Task Automation
This Theme comes with a built in gulp/webpack task automation. Sass files will be compiled if changed, vendor prefixes will be added automatically and the CSS will be minified. JS source files will be bundled and minified.

* Prerequisites: [Node.js](https://nodejs.org) (NPM) needs to be installed on your system
* Open the **Project directory** `/` in Terminal and install the required Node.js dependencies: gulp, webpack, Sass-Compiler, Autoprefixer, etc.
* `$ npm install`
* Run the **`watch`** script
* `$ npm run watch`
* Modify `/assets/main.scss`, `/assets/scss*` files and `/assets/main.js`


## Technology

* [Bootstrap](https://github.com/twbs/bootstrap), [MIT license](https://github.com/twbs/bootstrap/blob/master/LICENSE)
* [Sass](https://github.com/sass/sass), [MIT license](https://github.com/sass/sass/blob/stable/MIT-LICENSE)
* [gulp](https://github.com/gulpjs/gulp), [MIT license](https://github.com/gulpjs/gulp/blob/master/LICENSE)
* [webpack](https://github.com/webpack/webpack), [MIT license](https://github.com/webpack/webpack/blob/master/LICENSE)
* [wp-bootstrap-navwalker](https://github.com/twittem/wp-bootstrap-navwalker), [GPLv2+](https://github.com/twittem/wp-bootstrap-navwalker/blob/master/LICENSE.txt)

## Plugins

* Advanced Custom Fields (ACF) [https://www.advancedcustomfields.com](https://www.advancedcustomfields.com)