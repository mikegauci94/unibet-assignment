## UniBet Theme | Assignment by Michael Gauci | Kindred Group PLC

**README for the theme is located in the `unibet-theme` folder in `/wp-content/themes/unibet-theme/README.md`**

### How do I get set up ###

* git clone this repository in htdocs if you're using MAMP or WAMP to run it on your localhost
* In this repository you will find the sql dump for this project. 
* Create a new database in `mysql` with the credentials found in `wp-config.php` and import the sql dump
* Login credntials are U: admin P: admin
* Navigate to `/wp-content/themes/unibet-theme/`
* `$ npm install`
* `$ npm run watch` by watching the asset files

### Production ###

* [https://unibet.mikegauci.com](https://unibet.mikegauci.com)